#!/bin/bash
# tgen-server
# Run multiple parallel instances of TCPperf and iperf servers

# Sample use: sudo ./tgen-server
# Notes: use sudo ./tgen-kill after each run

###############################################################
#                                                             #
#        TCPperf Traffic Generation for dedupe data           #
#                                                             #
###############################################################                                                             #

# Ports used: (TCPperf can only use odd port numbers)
# CIFS - 445
# HTTP - 591
# HTTPS - 443
# Exchange - 135
# RDP - 3389
# Oracle - 1521
# Printer - 515
# Commvault - 8401
# DataDomain - 2051
# SnapMirror - 10565
#
# syntax: -s  --server [server_port [server_port [server_port]...]]

tcpperf -s 445 591 443 135 3389 1521 515 8401 2051 10565 &

###############################################################
#                                                             #
#      iperf Traffic Generation for non-dedupe data           #
#                                                             #
############################################################### 

# Ports used: (this script is only generating udp connections)
# SIP - 5060
# RTCP - 5005
# H.323 - 1718
#
# syntax: -s server -u udp -p port #
# Initialize base array application ports, modify as needed
declare -a app_ports=(5060 5005 1718)

# Run iperf multiple times
for i in `seq 0 2`; do

        App=$((app_ports[$i]));
        # Run iperf
        iperf -s -u -p $App &

done
