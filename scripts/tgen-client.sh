#!/bin/bash
# tgen-client
# Run multiple parallel instances of TCPperf and iperf clients

# Sample use: sudo ./tgen-client.sh
# Notes: use sudo ./tgen-kill after each run

###############################################################
#                                                             #
#        TCPperf Traffic Generation for dedupe data           #
#                                                             #
###############################################################
# syntax: 
#  -c <server IP> <TCP port>
#  -e Test length in Bytes
#  -y <s%> <m%> <l%> <sd> ...
#    s% = target NM reduction
#    m% = 0 <not used in this script>
#    l% = 100% - s% (e.g Tf target NM reduction is 75%, s% = 75 l% = 25)
#    sb = size of repeated pattern in MB (1-100 is the range) <this script uses 1MB>
#  -i test id (this is the seed used in random data generation)
#
# Example: send 100MB, target 80% reduction, send new data everytime script runs
#         tcpperf -c <server IP> <TCP port> -e 100000000 -y 80 0 20 1 -i $RANDOM
#
###############################################################

# Ports used: (TCPperf can only use odd port numbers)
# CIFS - 445 - 80%
# HTTP - 591 - 90%
# HTTPS - 443 - 0%
# Exchange - 135 - 70%
# RDP - 3389 - 50%
# Oracle - 1521 - 95%
# Printer - 515 - 85%
# Commvault - 8401 - 35%
# DataDomain - 2051 - 30%
# SnapMirror - 10565 - 83%
#

# -e values:
#
# 1GB = 1000000000
# 10GB = 10000000000
# 100GB = 100000000000
# 1TB = 1000000000000

###############################################################
#                                                             #
#                  Data Center Traffic                        #
#                                                             #
############################################################### 
# CIFS
tcpperf -c 10.0.10.10 445 -e 10000000000 -y 80 0 20 1 -i $RANDOM &
# Oracle
tcpperf -c 10.0.10.10 1521 -e 1000000000 -y 95 0 5 1 -i $RANDOM &
# DataDomain
tcpperf -c 10.0.20.10 2051 -e 50000000000 -y 30 0 70 1 -i $RANDOM &
# SnapMirror
tcpperf -c 10.0.20.10 10565 -e 50000000000 -y 83 0 17 1 -i $RANDOM &

###############################################################
#                                                             #
#                 Branch Office Traffic                       #
#                                                             #
############################################################### 
# Branch to Data Center Flows:
# CIFS - 445 - 80%
#tcpperf -c 10.0.10.10 445 -e 1000000000 -y 80 0 20 1 -i $RANDOM &

# HTTP - 591 - 90%
#tcpperf -c 10.0.10.10 591 -e 100000000 -y 90 0 10 1 -i $RANDOM &

# HTTPS - 443 - 0%
#tcpperf -c 10.0.10.10 443 -e 50000000 -y 0 0 100 1 -i $RANDOM &

# Exchange - 135 - 70%
#tcpperf -c 10.0.10.10 135 -e 750000000 -y 70 0 30 1 -i $RANDOM &

# RDP - 3389 - 50%
#tcpperf -c 10.0.10.10 3389 -e 25000000 -y 50 0 50 1 -i $RANDOM &

# Oracle - 1521 - 95%
#tcpperf -c 10.0.10.10 1521 -e 15000000 -y 95 0 5 1 -i $RANDOM &

# Printer - 515 - 85%
#tcpperf -c 10.0.10.10 515 -e 15000000 -y 85 0 15 1 -i $RANDOM &

# Commvault - 8401 - 35%
#tcpperf -c 10.0.20.10 8401 -e 5000000000 -y 35 0 65 1 -i $RANDOM &

###############################################################
#                                                             #
#      iperf Traffic Generation for non-dedupe data           #
#                                                             #
############################################################### 
# Ports used: (this script is only generating udp connections)
# SIP - 5060
# RTCP - 5005
# H.323 - 1718
#
# syntax: 
#  -c <server IP> - IP address to connect to 
#  -u use UDP
#  -p <Port#> - port to connect on

#iperf -c 10.0.30.10 -u -p 5060 -t 600 -b 64K -d &
#iperf -c 10.0.30.10 -u -p 5005 -t 600 -b 128K -d
#iperf -c 10.0.40.10 -u -p 1718 -t 600 -b 1M &