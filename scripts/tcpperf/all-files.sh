#!/bin/bash
#example
# test id = 1, 2M files: 10, 200M files: 10, 2G files: 1
#./all-files.sh 1 10 10 1 
# more data: start test id = total #of files generated before + 1
# test id = 22, 2M files: 10, 200M files: 10, 2G files: 1
#./all-files.sh 22 10 10 1 

# starting index in first param
index=$1
num2m=$2
num200m=$3
num2g=$4
type=$5

if [ $type eq "incremental" ]; then
  pool=100
else
  #pool=20
  pool=1000
fi

#dstdir="/tmp/"
dstdir="./tcpperf"##
:w
f X files between 0 and 2M bytes
filename=$dstdir"2M"
echo $filename

./batch.sh $filename 2000000 $num2m $index $pool

# Y files between 0 and 200M bytes
let "index += $num2m"
filename=$dstdir"200M"
echo $filename
./batch.sh $filename 200000000 $num200m $index $pool

# Z files between 0 and 2G bytes
let "index += $num200m"
filename=$dstdir"2G"
echo $filename

./batch.sh $filename 2000000000 $num2g $index $pool

