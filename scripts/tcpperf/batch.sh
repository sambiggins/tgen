#batch.sh:
#syntax
#./batch.sh <filename> <file size in bytes>  <number of files> <test/dataset id>
#           <short term memory pool in MB (use 1000 as def)
#example:
#./batch.sh /tmp/1-100Mfile 100000000 10 1 1000 
#
#generate 2nd set of unique files
#./batch.sh /tmp/1-100Mfile 100000000 10 2 1000

#!/bin/bash
NAME=$1
RANGE=$2
MAXCOUNT=$3
index=$4
pool=$5

count=1

while [ "$count" -le $MAXCOUNT ]
do

  number=$RANGE
  #comment out next 3 line to generate files of exact size, not 1 - max file size.
  #number=$RANDOM
  #let "number *= $RANGE"
  #let "number /= 32768"

  echo "Count=$count Filesize=$number index=$index"
  ./tcpperf -g $NAME -f calgary.zip -e $number -y 80 0 20 $pool -i $index
  let "index += 1"
  let "count += 1"
done

