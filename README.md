# README #

This is a web front-end for executing scripts on the traffic generator.

### Installation ###

You will need to install git to clone this repo. Python-pip to install dependencies.
```
#!bash
sudo apt-get update
sudo apt-get install git python-pip
git clone https://bitbucket.org/sambiggins/tgen.git
```
Virtualenv keeps python code separate from OS python.
```
#!bash
sudo pip install virtualenv
cd ~/tgen
virtualenv tgenenv
```
Activate the environment and install python dependencies
```
#!bash
source tgenenv/bin/activate
sudo pip install -r requirements.txt
```
The app can manually be started using the following command, which will make it available via http on port 8000. This uses gunicorn as the web server and is generally the preferred method.
```
#!bash
gunicorn --workers 3 --bind 0.0.0.0:8000 tgen:app
```

To enable running at startup copy (don't symlink!) the init/tgen.conf file to /etc/init/ NOTE: If you install as a different user (root for example) you will need to change the 'setuid' parameter to match the user, and change the path from /home/ubuntu to the correct user path for the env and chdir lines.
```
#!bash
sudo cp init/tgen.conf /etc/init
```
To start the app:
```
#!bash
sudo start tgen
```
To stop it:
```
#!bash
sudo stop tgen
```

For debugging purposes tgen.py can be run manually, which is then reachable via http on port 5000 (NOTE: This is meant for testing, and will instead use the Flask web server which is meant only for dev/ debugging purposes)

stdout and stderr from the shell script execution are logged to log.txt in the root folder.

### Assumptions ###

* init/tgen.conf file assumes the user is ubuntu
* iperf is already installed