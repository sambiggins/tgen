from flask import render_template, flash, redirect, request
from app import app
from .forms import IndexForm
import subprocess
from time import gmtime, strftime

global message
message = 'none'

def run_cmd(cmd):
	with open('log.txt', 'a') as fout:
		fout.write('%s Running %s\n' % (strftime("%Y-%m-%d %H:%M:%S", gmtime()), cmd))
		subprocess.Popen(cmd, stdout=fout,
	                          stderr=fout,
	                          stdin=subprocess.PIPE)

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
	global message
	form = IndexForm()
	if 'server' in request.form:
		cmd = ["scripts/tgen-server.sh"]
		run_cmd(cmd)
		message = 'server'
	elif 'client' in request.form:
		cmd = ["scripts/tgen-client.sh"]
		run_cmd(cmd)
		message = 'client'
	elif 'kill' in request.form:
		cmd = ["scripts/tgen-kill.sh"]
		run_cmd(cmd)
		message = 'kill'
	flash('Most recent action: %s' % message)
	return render_template('index.html', title='Home', form=form)